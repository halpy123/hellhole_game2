﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DeliveredSystem : MonoBehaviour
{
    public GameObject _scoreText;
    public static int TheScore = 0;

    public AudioSource tickSoucre;
   

    public Text _currentScore;
    public Text _bestScore;
    //TimerSystem bonusTime;
    //public AudioSource CollectSound;

    private void Start()
    {
        _bestScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();

        tickSoucre = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && ScoreSystem.PatientNum > 0)
        {
            tickSoucre.Play();

            TheScore += 50;
            _scoreText.GetComponent<Text>().text = "Score : " + TheScore;
            if (TheScore > PlayerPrefs.GetInt("HighScore", 0))
            {
                PlayerPrefs.SetInt("HighScore", TheScore);
                _bestScore.text = TheScore.ToString();
            }

            _currentScore.text = TheScore.ToString();
            ScoreSystem.PatientNum -= 1;
            //bonusTime.PlusTime();
            Debug.Log("PatientNum - 1");
        }
        
    }
}
