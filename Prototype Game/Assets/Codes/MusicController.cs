﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class MusicController : MonoBehaviour
{
    public AudioMixer Mixer;
     
    public void SetMusicVolume(float volume)
    {
        Mixer.SetFloat("MusicVol", volume);
    }
}
