﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool gameEnded = false;
    public GameObject Dashboard;

    public void EndGame()
    {
        if (gameEnded == false)
        {
        gameEnded = true;
        Debug.Log("Game Over");
            Time.timeScale = 0;
            Dashboard.SetActive(true);
        }
    }

    public void RestartGame()
    {
        Time.timeScale = 1;
        BarSrcipt.clean = 100;
        BarSrcipt.battery = 100;
        Dashboard.SetActive(false);
        Time.timeScale = 1;
        ScoreSystem.TheScore = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
