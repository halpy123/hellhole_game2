﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BarSrcipt : MonoBehaviour
{
    public static float clean = 100;
    public static float battery = 100;

    public float cleanOvertime;
    public float batteryOvertime;

    public Slider cleanBar;
    public Slider batteryBar;

    public float delayOvertime = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        cleanBar.maxValue = clean;
        batteryBar.maxValue = battery;
    }

    // Update is called once per frame
    void Update()
    {
        BatteryLife();
    }

    void BatteryLife()
    {
        clean -= cleanOvertime * Time.deltaTime;
        battery -= batteryOvertime * Time.deltaTime;

        updateUI();
    }

    void updateUI()
    {
        clean = Mathf.Clamp(clean, 0, 100f);
        battery = Mathf.Clamp(battery, 0, 100f);

        cleanBar.value = clean;
        batteryBar.value = battery;
    }
}
