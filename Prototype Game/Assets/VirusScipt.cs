﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirusScipt : MonoBehaviour
{
    public AudioSource m_MyAudioSource;
    void Start()
    {
        m_MyAudioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_MyAudioSource.Play();
            BarSrcipt.clean -= 20;
            BarSrcipt.battery -= 10;
        }
    }
}
